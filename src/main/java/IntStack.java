class IntStack
{
    private int[] arr;
    private int top;
    private int capacity;

    /**
     * Constructor to initialize the stack
     */
    IntStack(int size) {
        arr = new int[size];
        capacity = size;
        top = -1;
    }

    /**
     * Adds an element `x` to the stack
     */
    public void push(int x) {
        System.out.println("Inserting " + x);
        arr[++top] = x;
    }

    /**
     * Pops a top element from the stack
     */
    public int pop() {
        // check for stack underflow
        if (isEmpty()) {
            throw new ArrayIndexOutOfBoundsException("Underflow");
        }

        System.out.println("Removing " + peek());

        // decrease stack size by 1 and (optionally) return the popped element
        return arr[top--];
    }

    /**
     * Returns the top element of the stack
     */
    public int peek() {
        if (!isEmpty()) {
            return arr[top];
        }
        else {
            throw new ArrayIndexOutOfBoundsException("Stack is empty");
        }
    }

    /**
     * Returns the size of the stack
     */
    public int size() {
        return top + 1;
    }

    /**
     * Checks if the stack is empty or not
     */
    public Boolean isEmpty() {
        return top == -1;               // or return size() == 0;
    }

    /**
     * Checks if the stack is full or not
     */
    public Boolean isFull() {
        return top == capacity - 1;
    }
}